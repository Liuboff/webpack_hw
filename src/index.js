import './styles/index.scss';
import $ from 'jquery';

const userStack = {
  language: 'JS',
  framework: 'Angular',
};

const user = {
  name: 'Liuboff',
  job: 'student',
  ...userStack,
};
$('.block').html('jQuery is working');

console.log(user);
